import React, { Component } from 'react';

import { StyleSheet, View, Text, ScrollView, TextInput } from 'react-native';
import { Button, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.body}>
          <View style={styles.logo}><Text>Imagem aqui</Text></View>
          <View style={[styles.form, styles.blurBackground]}>
            <Text style={styles.title}>Titulo aqui</Text>
            <Text style={styles.warning}>Mensagem aqui</Text>
            <View style={styles.inputInline}>
              <View style={styles.loginSection}>
                <Icon style={styles.spaceIcon} name="user" size={20} color="#000"/>
                <TextInput
                  keyboardType="email-address"
                  style={styles.input}
                  placeholder="Digite seu email aqui"
                  onChangeText={(userLogin) => {this.setState({userLogin})}}
                  underlineColorAndroid="transparent"
                />
              </View>

              <View style={styles.loginSection}>
                <Icon style={styles.spaceIcon} name="lock" size={20} color="#000"/>
                <TextInput
                  secureTextEntry={true}
                  style={styles.input}
                  placeholder="Digite sua senha aqui"
                  onChangeText={(userPassword) => {this.setState({userPassword})}}
                  underlineColorAndroid="transparent"
                />
              </View>

              <Button title="Entrar" />
            </View>




          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  form: {
    alignSelf: 'stretch',
  },
  blurBackground:{

  },
  logo: {

  },
  title:{
    alignSelf: 'center'
  },
  warning:{
    alignSelf: 'center'
  },
  loginSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    // borderWidth: 1,
    // borderColor: '#ccc',
    marginBottom: 5,
    backgroundColor: '#fff'
  },
  spaceIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#424242',
  },
});
