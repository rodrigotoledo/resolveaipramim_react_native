import React, { Component } from 'react';

import { StyleSheet, View, Text, ScrollView, TextInput, Dimensions, ImageBackground } from 'react-native';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import api from '../../services/api';

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {

    return (
      <ImageBackground style={{ width: '100%', height: '100%', }} source={require('../../../assets/images/background.jpg')}>
        <View style={styles.mainContainer}>

        <ScrollView contentContainerStyle={{flexGrow : 1, justifyContent : 'center'}} style={styles.container}>
          <View style={styles.body}>
            <View style={[styles.form, styles.blurBackground]}>
              <Text style={styles.title}>Resolve ai pra MIM!</Text>
              <View style={styles.inputInline}>
                <View style={styles.loginSection}>
                  <Icon style={styles.spaceIcon} name="user" size={20} color="#000"/>
                  <TextInput
                    keyboardType="email-address"
                    style={styles.input}
                    placeholder="Digite seu email aqui"
                    onChangeText={(userLogin) => {this.setState({userLogin})}}
                    underlineColorAndroid="transparent"
                  />
                </View>

                <View style={styles.loginSection}>
                  <Icon style={styles.spaceIcon} name="lock" size={20} color="#000"/>
                  <TextInput
                    secureTextEntry={true}
                    style={styles.input}
                    placeholder="Digite sua senha aqui"
                    onChangeText={(userPassword) => {this.setState({userPassword})}}
                    underlineColorAndroid="transparent"
                  />
                </View>

                <Button title="Entrar" />
              </View>
            </View>
          </View>
        </ScrollView>

        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  container: {
    height: Dimensions.get('window').height,
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    borderColor: '#0e7385',
    borderWidth: 10,
    borderRadius: 45,
    padding: 20,
    margin: 20
  },
  form: {
    alignSelf: 'stretch',

  },
  blurBackground:{

  },
  logo: {

  },
  title:{
    alignSelf: 'center',
    fontSize: 40,
    fontFamily: 'Komiko'
  },
  warning:{
    alignSelf: 'center'
  },
  loginSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    marginBottom: 5,
    backgroundColor: '#fff'
  },
  spaceIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#424242',
  },
});
