import { StackNavigator, TabNavigator } from 'react-navigation';

import Login from './pages/login';
import Main from './pages/main';
import Dashboard from './pages/dashboard';

const Routes = StackNavigator({
  Login: { screen: Login },
  Logged: {
    screen: TabNavigator({
      Main: { screen: Main },
      Dashboard: { screen: Dashboard },
    }),
  },
}, {
  initialRouteName: 'Login',
});

export default Routes;
