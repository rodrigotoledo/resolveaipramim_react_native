/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import './config/ReactotronConfig';


// import { View } from 'react-native';
import Routes from './routes';


export default class App extends Component {

  render() {
    return (
      <Routes />
    );
  }
}
